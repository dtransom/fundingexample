﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GiftAidCalculator.TestConsole
{
    using GiftAidCalculator.TestConsole.Services.GetGiftAidRate;

    public class DonorUI
    {
        private IGetGiftAidRateService giftAidRateService;

        public DonorUI(IGetGiftAidRateService giftAidRateService)
        {
            this.giftAidRateService = giftAidRateService;
        }

        public bool Run(string username)
        {
            Console.WriteLine(
                $"Thanks {username}, Please enter an amount to view how much gift aid you could get");
            Console.WriteLine(this.giftAidRateService.GetGiftAid(decimal.Parse(Console.ReadLine())));
            Console.WriteLine("Thanks! Press any key to exit.");
            Console.ReadLine();
            return false;
        }
    }
}
