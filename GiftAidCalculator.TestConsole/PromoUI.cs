﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GiftAidCalculator.TestConsole
{
    public class PromoUI
    {
        // Think of better name
        public bool Run()
        {
            Console.WriteLine($"Promo Time!!!! What event we looking to promote??");

            //// Should go into a db somewhere but feel like enough of the pattern has been demonstated so for now just going to hardcode a response back to user

            var eventType = Console.ReadLine();
            if (eventType != null)
            {
                if (eventType.ToLower() == "running")
                {
                    Console.WriteLine("5% added");
                }
                if (eventType.ToLower() == "swimming")
                {
                    Console.WriteLine("3% added");
                }
                else
                {
                    Console.WriteLine("nothing added :(");
                }
            }

            Console.WriteLine("Thanks! Press any key to exit.");
            Console.ReadLine();
            return false;
        }
    }
}
