﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GiftAidCalculator.TestConsole
{
    using GiftAidCalculator.TestConsole.Enums;
    using GiftAidCalculator.TestConsole.Repository;
    using GiftAidCalculator.TestConsole.Services.GetGiftAidRate;
    using GiftAidCalculator.TestConsole.Services.Login;
    using GiftAidCalculator.TestConsole.Services.UpdateTaxRate;

    public class JustGivingUI
    {
        private ILoginService loginService;

        public JustGivingUI(ILoginService loginService)
        {
            this.loginService = loginService;
        }

        public void Run()
        {
            bool repeatSteps = true;

            while (repeatSteps)
            {
                Console.WriteLine("Please Enter Your Username:");
                var username = Console.ReadLine();
                var role = this.loginService.GetUserRole(username);
                if (role == UserRole.Promotor)
                {
                    repeatSteps = new PromoUI().Run();
                }

                if (role == UserRole.Donor)
                {
                    repeatSteps = new DonorUI(new GetGiftAidRateService()).Run(username);
                }

                if (role == UserRole.Admin)
                {
                    repeatSteps = new AdminUI(new TaxRateService(new StubbedTaxRepository())).Run(username);
                }
                if (role == UserRole.Unknown)
                {
                    Console.WriteLine("Invalid role, do you want to try again? enter Y/N");
                    var option = Console.ReadLine();
                    if (option.ToUpper() != "Y")
                    {
                        repeatSteps = false;
                    }
                }
            }
        }
    }
}
