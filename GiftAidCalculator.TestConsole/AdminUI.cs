﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GiftAidCalculator.TestConsole
{
    using GiftAidCalculator.TestConsole.Services.GetGiftAidRate;
    using GiftAidCalculator.TestConsole.Services.UpdateTaxRate;

    public class AdminUI
    {
        private IUpdateTaxRate updateTaxRateService;

        public AdminUI(IUpdateTaxRate updateTaxRateService)
        {
            this.updateTaxRateService = updateTaxRateService;
        }

        public bool Run(string username)
        {
            Console.WriteLine("Would you like to amend the tax rate? enter Y/N");
            var option = Console.ReadLine();
            if (option.ToUpper() == "Y")
            {
                Console.WriteLine(
                    $"Thanks {username}, Please how much tax rate should be now");

                var amount = decimal.Parse(Console.ReadLine());
                var result = this.updateTaxRateService.UpdateTaskRate(amount);
                if (!result)
                {
                    Console.WriteLine("Please Login and Try again.");
                    return true;
                }
                else
                {
                    Console.WriteLine($"Thanks, Tax rate is now {amount}. Press any key to exit");
                    Console.ReadLine();
                    return false;
                }
            }

            Console.WriteLine("OK BYE!.");
            return false;
        }
    }
}
