﻿namespace GiftAidCalculator.TestConsole.Enums
{
    public enum UserRole
    {
        Unknown = 0,

        Donor = 1,

        Promotor = 2,

        Admin = 3
    }
}
