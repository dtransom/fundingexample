﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GiftAidCalculator.TestConsole.Repository
{
    public class UserRepo : IUserRepo
    {
        public int LoginUser(string creds)
        {
            switch (creds)
            {
                case "Dan":
                     return 3;
                case "Mary":
                    return 2;
                case "Clive":
                    return 1;
                default:
                    return 0;
            }
        }
    }
}
