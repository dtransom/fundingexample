﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GiftAidCalculator.TestConsole.Repository
{
    public class StubbedTaxRepository : ITaxRepository
    {
        public decimal GetTax()
        {
            return 20.0m;
        }

        public bool UpdateTax(decimal TaxValue)
        {
            if (TaxValue == 89.0m)
            {
                return false;
            }

            return true;
        }
    }
}
