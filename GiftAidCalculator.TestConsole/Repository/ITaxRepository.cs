﻿namespace GiftAidCalculator.TestConsole.Repository
{
    public interface ITaxRepository
    {
        decimal GetTax();

        bool UpdateTax(decimal TaxValue);
    }
}