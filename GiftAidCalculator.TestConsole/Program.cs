﻿namespace GiftAidCalculator.TestConsole
{
    using GiftAidCalculator.TestConsole.Repository;
    using GiftAidCalculator.TestConsole.Services.GetGiftAidRate;
    using GiftAidCalculator.TestConsole.Services.Login;
    using GiftAidCalculator.TestConsole.Services.UpdateTaxRate;

    class Program
    {
        static void Main(string[] args)
        {
            //// Would normally inject the interface using an IoC container using something like StructureMap
            new JustGivingUI(
                new LoginService(new UserRepo())).Run();
        }
    }
}