﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GiftAidCalculator.TestConsole.Services.UpdateTaxRate
{
    using GiftAidCalculator.TestConsole.Repository;

    public class TaxRateService : IUpdateTaxRate
    {
        private ITaxRepository repo;

        public TaxRateService(ITaxRepository repo)
        {
            this.repo = repo;
        }


        public bool UpdateTaskRate(decimal TaxRate)
        {
            var result = this.repo.UpdateTax(TaxRate);
            return result;
        }
    }
}
