﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GiftAidCalculator.TestConsole.Services.UpdateTaxRate
{
    public interface IUpdateTaxRate
    {
        bool UpdateTaskRate(decimal taxRate);
    }
}
