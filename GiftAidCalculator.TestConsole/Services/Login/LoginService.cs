﻿namespace GiftAidCalculator.TestConsole.Services.Login
{
    using GiftAidCalculator.TestConsole.Enums;
    using GiftAidCalculator.TestConsole.Repository;

    public class LoginService : ILoginService
    {
        /// <summary>
        /// I realise for this example that having a service and repo is overkill but doing it for design patter/Knowledge in Moq demonstration
        /// </summary>
        private IUserRepo repo;

        public LoginService(IUserRepo repo)
        {
            this.repo = repo;
        }

        public UserRole GetUserRole(string userName)
        {
            var loginStatus = this.repo.LoginUser(userName);
            return (UserRole)loginStatus;
        }
    }
}
