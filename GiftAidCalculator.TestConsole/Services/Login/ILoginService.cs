﻿namespace GiftAidCalculator.TestConsole.Services.Login
{
    using GiftAidCalculator.TestConsole.Enums;

    public interface ILoginService
    {
        UserRole GetUserRole(string userName);
    }
}