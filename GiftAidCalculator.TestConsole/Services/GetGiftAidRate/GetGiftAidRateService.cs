﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GiftAidCalculator.TestConsole.Services.GetGiftAidRate
{
    public class GetGiftAidRateService : IGetGiftAidRateService
    {
        public decimal GetGiftAid(decimal value)
        {
            var gaRatio = 20.0m / (100 - 20.0m);
            return Math.Round(value * gaRatio, 2);
        }
    }
}
