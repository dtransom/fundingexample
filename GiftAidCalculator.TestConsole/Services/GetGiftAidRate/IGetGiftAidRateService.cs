﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GiftAidCalculator.TestConsole.Services.GetGiftAidRate
{
    public interface IGetGiftAidRateService
    {
        decimal GetGiftAid(decimal value);
    }
}
