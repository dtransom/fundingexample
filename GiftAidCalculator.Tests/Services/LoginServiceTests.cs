﻿namespace GiftAidCalculator.Tests.Services
{
    using GiftAidCalculator.TestConsole.Enums;
    using GiftAidCalculator.TestConsole.Repository;
    using GiftAidCalculator.TestConsole.Services.Login;

    using Moq;

    using NUnit.Framework;

    [TestFixture]
    public class LoginServiceTests
    {
        [Test]
        public void TestAdminUser()
        {
            // Arrange
            var mockUserRepo = new Mock<IUserRepo>();
            mockUserRepo.Setup(x => x.LoginUser(It.IsAny<string>())).Returns(3);

            var service = new LoginService(mockUserRepo.Object);

            // Act
            var result = service.GetUserRole(It.IsAny<string>());

            // Assert
            Assert.AreEqual(result, UserRole.Admin);
        }


        [TestCase(UserRole.Donor, 1)]
        [TestCase(UserRole.Promotor, 2)]
        public void TestOtherUsers(UserRole role, int dbValue)
        {
            // Arrange
            var mockUserRepo = new Mock<IUserRepo>();
            mockUserRepo.Setup(x => x.LoginUser(It.IsAny<string>())).Returns(dbValue);

            var service = new LoginService(mockUserRepo.Object);

            // Act
            var result = service.GetUserRole(It.IsAny<string>());

            // Assert
            Assert.AreEqual(result, role);
        }

        [Test]
        public void TestUnknownUser()
        {
            // Arrange
            var mockUserRepo = new Mock<IUserRepo>();
            mockUserRepo.Setup(x => x.LoginUser(It.IsAny<string>())).Returns(0);

            var service = new LoginService(mockUserRepo.Object);

            // Act
            var result = service.GetUserRole(It.IsAny<string>());

            // Assert
            Assert.AreEqual(result, UserRole.Unknown);
        }
    }
}
