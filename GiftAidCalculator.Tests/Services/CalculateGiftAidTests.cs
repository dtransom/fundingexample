﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GiftAidCalculator.Tests.Services
{
    using GiftAidCalculator.TestConsole.Services.GetGiftAidRate;

    using NUnit.Framework;

    [TestFixture]
    public class CalculateGiftAidTests
    {
        private GetGiftAidRateService giftaidservice;

        [SetUp]
        public void Setup()
        {
            this.giftaidservice = new GetGiftAidRateService();
        }

        [Test]
        public void ShouldReturnExpectedGiftAidValueAt20PercentTax()
        {
            // Arrange
            var enteredValue = new decimal(15.00);
            var expectedResult = new decimal(3.75);

            // Act
            var result = this.giftaidservice.GetGiftAid(enteredValue);

            // Assert
            Assert.AreEqual(result, expectedResult);
        }

        [Test]
        public void ShouldReturnCorrectAmountOfDecimalPlaces()
        {
            // Arrange
            var enteredValue = new decimal(162.35478);
            var expectedResult = new decimal(40.59);

            // Act
            var result = this.giftaidservice.GetGiftAid(enteredValue);

            // Assert
            Assert.AreEqual(result, expectedResult);
        }
    }
}
