﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GiftAidCalculator.Tests.Services
{
    using GiftAidCalculator.TestConsole.Enums;
    using GiftAidCalculator.TestConsole.Repository;
    using GiftAidCalculator.TestConsole.Services.Login;
    using GiftAidCalculator.TestConsole.Services.UpdateTaxRate;

    using Moq;

    using NUnit.Framework;

    [TestFixture]
    public class TaxRateServiceTests
    {
        [Test]
        public void TestBad()
        {
            // Arrange
            var mockTaxRepo = new Mock<ITaxRepository>();
            mockTaxRepo.Setup(x => x.UpdateTax(It.IsAny<decimal>())).Returns(false);

            var service = new TaxRateService(mockTaxRepo.Object);

            // Act
            var result = service.UpdateTaskRate(It.IsAny<decimal>());

            // Assert
            Assert.IsFalse(result);
        }

        [Test]
        public void TestGood()
        {
            // Arrange
            var mockTaxRepo = new Mock<ITaxRepository>();
            mockTaxRepo.Setup(x => x.UpdateTax(It.IsAny<decimal>())).Returns(true);

            var service = new TaxRateService(mockTaxRepo.Object);

            // Act
            var result = service.UpdateTaskRate(It.IsAny<decimal>());

            // Assert
            Assert.IsTrue(result);
        }
    }
}
