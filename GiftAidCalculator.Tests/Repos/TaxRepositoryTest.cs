﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GiftAidCalculator.Tests.Repos
{
    using GiftAidCalculator.TestConsole.Repository;

    using NUnit.Framework;

    [TestFixture]
    public class TaxRepositoryTest
    {
        [Test]
        public void TestRepoUpdates()
        {
            // Arrange
            var repo = new StubbedTaxRepository();

            // Act
            var result = repo.UpdateTax(90.0m);

            // Assert
            Assert.IsTrue(result);
        }

        [Test]
        public void TestRepoDoesNotUpdate()
        {
            // Arrange
            var repo = new StubbedTaxRepository();

            // Act
            var result = repo.UpdateTax(89.0m);

            // Assert
            Assert.IsFalse(result);
        }
    }
}
